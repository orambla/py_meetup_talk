# -*- coding: utf-8 -*-

"""
This utility is taken from Tarek Ziadé blog:
   http://blog.ziade.org/2012/05/25/zmq-and-gevent-debugging-nightmares/

It provides with the active greenlets stacks by exploring the gc.
Could this be done a fancier way?
"""

import traceback, sys, gc
import gevent.local

def dump_stacks():
    dump = []

    # greenlets
    try:
        from greenlet import greenlet
    except ImportError:
        return dump

    # if greenlet is present, let's dump each greenlet stack
    for ob in gc.get_objects():
        if not isinstance(ob, greenlet):
            continue
        if not ob:
            continue   # not running anymore or not started
        dump.append('Greenlet\n')
        dump.append(''.join(traceback.format_stack(ob.gr_frame)))
        dump.append('\n')

    return dump
