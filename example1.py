"""
Example 1: Basic usages: 
  Run N greenlets within a timeout with different tasks.
  Are they concurrrent?
Usage:
  python example1.py task[123] N
"""

import sys
import time

start_time = 0.0
total_times = 0

def run( id, task):
    """
    A basic profiling wrapper around a task repeatedly executed.
    """
    from gevent import sleep

    global total_times, start_time

    times = 0
    g_time = 0.0
    while True:
       gs_time = time.time()
       task()
       g_time += time.time() - gs_time
       times += 1
       total_times += 1
       print( "id: %s, times: %s, total_times: %s, g_time: %s, runtime: %s" % (id, times, total_times, g_time, time.time()-start_time))
       sleep(0)

    return times

def example1(task, n_jobs=3):
    """ 
    Launch n greenlet jobs within a timeout
    """
    from gevent import joinall, spawn

    global start_time, total_times

    total_times = 0
    start_time = time.time()
    greenlets = [ spawn(run, n, task) for n  in range(n_jobs) ]
    joinall(greenlets, timeout=10)
    print("n_jobs: %s, total_times: %s, runtime: %s" % (n_jobs, total_times, time.time()-start_time))


def task1():
    """ 
    Blocking and non blocking sleep
    Usage: Uncomment alternatively standard and gevent sleep import
           and compare results.
    """
    from gevent import sleep
    # from time import sleep

    sleep(1)


def task2():
    """ 
    CPU bound. Can't be made concurrent with greenlets.
    """
    import math

    math.factorial(3e+4) 


def task3():
    """ 
    Patched and unpatched urllib calls.
    Usage: Comment patch call and compare results.
    """
    from gevent import sleep
    from gevent.monkey import patch_socket
    patch_socket()

    import urllib2

    urls = [ 
       "http://www.meetup.com/python-185/",
       "http://www.meetup.com/python-185/events/64046582/",
       "http://www.meetup.com/python-185/events/69308302/",
]

    url = urls[total_times % len(urls)]
    urllib2.urlopen(url).read()

if __name__=='__main__':
    example1(globals()[sys.argv[1]], int(sys.argv[2]))
