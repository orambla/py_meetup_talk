"""
Example 2: Implements a Web interface around a process allowing some interoperability and monitoring at run-time.
Usage:
    python example2.py

Web Usage:
    http://localhost:8888/          Show greenlet workers status list
    http://localhost:8888/start     Calls a client that creates n greenlet workers
    http://localhost:8888/kill/n    Kills greenlet n by throwing an exception 
    http://localhost:8888/stop      Kill all greenlet workers
    http://localhost:8888/dump      Shows greenlets stacks
"""

import sys
import traceback
import json
import time

from gevent import sleep, wsgi, joinall, spawn, killall
from gevent import getcurrent
from gevent.greenlet import Greenlet
from gevent.local import local
from gevent.monkey import patch_all

patch_all()

import urllib2

from util import dump_stacks

PORT=8888

def run(n_worker):
    """ 
    A task expected to be run inside a greenlet.
    Shows usage of local.
    """
    local_data = local()
    local_data.n_worker = n_worker
    local_data.start_time = time.time()
    for n in range(20):
        local_data.n = n
        print local_data.__dict__
        sleep(1)

    return n

def example2():
    greenlets=[]

    def client():
        """ 
        Makes requests for new workers through ws.
        """
        n=0
        while True:
            resp = urllib2.urlopen("http://localhost:%s/worker/%s" % (PORT, len(greenlets)) )
            print resp.read()
            sleep(1)

    def worker(n):
        """ 
        Use Greenlet API to start a greenlet
        """
        glet = Greenlet( run, n )
        greenlets.append( glet )
        glet.start()
        return glet

    def dump_greenlets():
        """
        List greenlets properties.
        """
        return ["glet: %r value: %s ready: %s exc %s\n" % \
                (glt, glt.value, glt.ready(), glt.exception)
                for glt in greenlets]

    def app(env, start_response):
        """ 
        A wsgi application
        """
        path = env['PATH_INFO'].split('/')[1:]

        try:
            if not path[0]:
                start_response('200 OK', [('Content-Type', 'text/plain')])
                return dump_greenlets()

            elif path[0]=='start':
                greenlets.append( spawn(client) )
                sleep(0.2)
                start_response('200 OK', [('Content-Type', 'text/plain')])
                return dump_greenlets()

            elif path[0]=='worker':
                n_worker = int(path[1])
                start_response('200 OK', [('Content-Type', 'text/plain')])
                return [ str(worker(n_worker)),]

            elif path[0]=='kill':
                n_worker = int(path[1])
                greenlets[n_worker].kill(ValueError("Killed badly."))
                sleep(0.2)
                start_response('200 OK', [('Content-Type', 'text/plain')])
                return dump_greenlets()

            elif path[0]=='stop':
                killall(greenlets)
                sleep(0.2)
                start_response('200 OK', [('Content-Type', 'text/plain')])
                return dump_greenlets()

            elif path[0]=='dump':
                start_response('200 OK', [('Content-Type', 'text/plain')])
                return dump_stacks()

            else:
                start_response('404 Not Found', [('Content-Type', 'text/plain')])
                return ["Nop."]

        except Exception,e:
            print("App exception '%s'" % e)
            start_response('500 Internal Error', [('Content-Type', 'text/html')])
            return [ '<h1>Internal Error</h1>',
                     str(traceback.format_exc()),
                     '%r' % (env) ]


    try:
        wsgi.WSGIServer(('', PORT), app).serve_forever()

    except (KeyboardInterrupt, SystemExit),exception:
        print("Exit exc %r'" % (exception))

    except Exception,exception:
        print('Exit'+''.join(traceback.format_exception(*sys.exc_info())))


if __name__=='__main__':
    example2()
